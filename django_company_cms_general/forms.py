from django_recaptcha.fields import ReCaptchaField
from django_recaptcha.widgets import ReCaptchaV3
from django import forms
from django.utils.translation import gettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField


class GenericContactForm(forms.Form):
    name = forms.CharField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "class": "form-control ps-5",
                "placeholder": _("Your Name"),
            }
        ),
    )
    from_email = forms.EmailField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "class": "form-control ps-5",
                "placeholder": _("Your Email Address"),
            }
        ),
    )
    phone = PhoneNumberField(
        required=True,
        widget=forms.TextInput(
            attrs={
                "class": "form-control ps-5",
                "placeholder": _("Your Phone No."),
            }
        ),
    )
    message = forms.CharField(
        required=False,
        widget=forms.Textarea(
            attrs={
                "class": "form-control ps-5",
                "rows": 2,
                "cols": 15,
                "placeholder": _("Please contact me to discuss my wishes."),
            }
        ),
    )
    captcha = ReCaptchaField(widget=ReCaptchaV3)


class ContractTerminationForm(forms.Form):
    name = forms.CharField(
        required=True,
        label=_("Your Full Name or Company Name"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control col-lg-6",
                "placeholder": _("Your Full Name or Company Name"),
            }
        ),
    )
    email = forms.EmailField(
        required=True,
        label=_("E-Mail"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "placeholder": _("Your Email Address"),
            }
        ),
    )
    customer_number = forms.CharField(
        required=True,
        label=_("Customer Number"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control col-lg-6",
                "placeholder": _("Your Customer Number"),
            }
        ),
    )
    contract_number = forms.CharField(
        required=True,
        label=_("Contract Number"),
        widget=forms.TextInput(
            attrs={
                "class": "form-control col-lg-6",
                "placeholder": _("Contract Number to terminate"),
            }
        ),
    )

    message = forms.CharField(
        required=False,
        label=_("Your Message"),
        widget=forms.Textarea(
            attrs={
                "class": "form-control",
                "rows": 4,
                "cols": 15,
                "placeholder": _("Your Message"),
            }
        ),
    )

    captcha = ReCaptchaField(label="", widget=ReCaptchaV3)
