from urllib.parse import quote_plus

from django.contrib import messages
from django.http import (
    HttpResponse,
    HttpResponseNotFound,
    HttpResponseBadRequest,
    HttpResponseForbidden,
    HttpResponseServerError,
)
from django.template import loader
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.forms import (
    ContractTerminationForm,
)
from django_company_cms_general.django_company_cms_general.func import (
    send_contract_termination_ticket,
)
from django_company_cms_general.django_company_cms_general.models import *


def home(request):
    template = loader.get_template("django_company_cms_general/home.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = CompanyCmsGeneral.objects.get(
        name="company_slogan_short"
    ).content

    template_opts["is_home_page"] = True

    template_opts["company_slogan_main"] = CompanyCmsGeneral.objects.get(
        name="company_slogan_main"
    ).content
    template_opts["company_slogan_short"] = CompanyCmsGeneral.objects.get(
        name="company_slogan_short"
    ).content
    template_opts["company_slogan_long"] = CompanyCmsGeneral.objects.get(
        name="company_slogan_long"
    ).content

    template_opts["home_contact_long_text"] = CompanyCmsGeneral.objects.get(
        name="home_contact_long_text"
    ).content
    template_opts["home_contact_short_text"] = CompanyCmsGeneral.objects.get(
        name="home_contact_short_text"
    ).content

    template_opts["company_testimonials"] = CompanyCmsTestimonial.objects.all()

    template_opts["company_people_01_name"] = CompanyCmsGeneral.objects.get(
        name="company_people_01_name"
    ).content
    template_opts["company_people_02_name"] = CompanyCmsGeneral.objects.get(
        name="company_people_02_name"
    ).content
    template_opts["company_people_03_name"] = CompanyCmsGeneral.objects.get(
        name="company_people_03_name"
    ).content

    template_opts["company_people_01_title"] = CompanyCmsGeneral.objects.get(
        name="company_people_01_title"
    ).content
    template_opts["company_people_02_title"] = CompanyCmsGeneral.objects.get(
        name="company_people_02_title"
    ).content
    template_opts["company_people_03_title"] = CompanyCmsGeneral.objects.get(
        name="company_people_03_title"
    ).content

    template_opts["company_key_topic_01_name"] = CompanyCmsGeneral.objects.get(
        name="company_key_topic_01_name"
    ).content
    template_opts["company_key_topic_01_text"] = CompanyCmsGeneral.objects.get(
        name="company_key_topic_01_text"
    ).content
    template_opts["company_key_topic_02_name"] = CompanyCmsGeneral.objects.get(
        name="company_key_topic_02_name"
    ).content
    template_opts["company_key_topic_02_text"] = CompanyCmsGeneral.objects.get(
        name="company_key_topic_02_text"
    ).content

    template_opts["company_history_read_more_btn"] = CompanyCmsGeneral.objects.get(
        name="company_history_read_more_btn"
    ).content

    template_opts["company_services_header"] = CompanyCmsGeneral.objects.get(
        name="company_services_header"
    ).content
    template_opts["company_services_text"] = CompanyCmsGeneral.objects.get(
        name="company_services_text"
    ).content

    for i in range(1, 10):
        str_num = str(i).zfill(2)

        template_opts["company_expert_topic_" + str_num + "_name"] = (
            CompanyCmsGeneral.objects.get(
                name="company_expert_topic_" + str_num + "_name"
            ).content
        )

        template_opts["company_expert_topic_" + str_num + "_icon"] = (
            CompanyCmsGeneral.objects.get(
                name="company_expert_topic_" + str_num + "_icon"
            ).content
        )

    template_opts["company_slogan_second"] = CompanyCmsGeneral.objects.get(
        name="company_slogan_second"
    ).content
    template_opts["company_description_long"] = CompanyCmsGeneral.objects.get(
        name="company_description_long"
    ).content

    template_opts["company_testimonial_header"] = CompanyCmsGeneral.objects.get(
        name="company_testimonial_header"
    ).content

    template_opts["company_topics"] = list()

    for i in range(1, 5):
        template_opts["company_topics"].append(
            {
                "title": CompanyCmsGeneral.objects.get(
                    name="company_topic_title_" + str(i)
                ).content,
                "description": CompanyCmsGeneral.objects.get(
                    name="company_topic_description_" + str(i)
                ).content,
                "icon": CompanyCmsGeneral.objects.get(
                    name="company_topic_icon_" + str(i)
                ).content,
            }
        )

    return HttpResponse(template.render(template_opts, request))


def customer_support_guide(request):
    template = loader.get_template(
        "django_company_cms_general/customer_support_guide.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("Customer Support Guide")

    return HttpResponse(template.render(template_opts, request))


def imprint(request):
    template = loader.get_template("django_company_cms_general/imprint.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("Imprint")

    imprint_content = CompanyCmsGeneral.objects.get(name="imprint")

    template_opts["imprint_content"] = imprint_content.content

    return HttpResponse(template.render(template_opts, request))


def privacy_policy(request):
    template = loader.get_template("django_company_cms_general/privacy_policy.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("Data Protection (Privacy Statement)")

    privacy_policy_content = CompanyCmsGeneral.objects.get(name="privacy_policy")

    template_opts["privacy_policy_content"] = privacy_policy_content.content

    return HttpResponse(template.render(template_opts, request))


def terms_and_conditions(request):
    template = loader.get_template(
        "django_company_cms_general/terms_and_conditions.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("General Terms and Conditions")

    terms_and_conditions_content = CompanyCmsGeneral.objects.get(
        name="terms_and_conditions"
    )

    template_opts["terms_and_conditions_content"] = terms_and_conditions_content.content

    return HttpResponse(template.render(template_opts, request))


def contact(request):
    template = loader.get_template("django_company_cms_general/contact.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("Contact")

    return HttpResponse(template.render(template_opts, request))


def web_directory(request):
    template = loader.get_template("django_company_cms_general/web_directory.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Company")
    template_opts["content_title_sub"] = _("Web Directory")

    template_opts["web_directory_list"] = CompanyCmsWebDirectory.objects.all()

    return HttpResponse(template.render(template_opts, request))


def page_not_found(request, exception="Page not found"):
    template = loader.get_template("django_company_cms_general/404.html")

    path_info = request.path_info

    template_opts = dict()

    template_opts["content_title_main"] = _("Oops!")
    template_opts["content_title_sub"] = _("Page not found")

    template_opts["search_query"] = quote_plus(
        path_info.replace("-", " ").replace("/", " ").strip()
    )

    return HttpResponseNotFound(template.render(template_opts, request))


def server_error(request):
    template = loader.get_template("django_company_cms_general/500.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Oops!")
    template_opts["content_title_sub"] = _("Something went wrong")

    return HttpResponseServerError(template.render(template_opts, request))


def permission_denied(request, exception="Permission Denied"):
    template = loader.get_template("django_company_cms_general/403.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Oops!")
    template_opts["content_title_sub"] = _("Permission Denied")

    return HttpResponseForbidden(template.render(template_opts, request))


def bad_request(request, exception="Something went wrong"):
    template = loader.get_template("django_company_cms_general/400.html")

    template_opts = dict()

    template_opts["content_title_main"] = _("Oops!")
    template_opts["content_title_sub"] = _("Something went wrong")

    return HttpResponseBadRequest(template.render(template_opts, request))


def contract_termination(request):
    # TODO: fix link to contracts in customer center
    template = loader.get_template(
        "django_company_cms_general/contract_termination.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Contracts")
    template_opts["content_title_sub"] = _("Contract Termination")

    template_opts["contract_termination_01_header"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_01_header"
    ).content

    template_opts["contract_termination_01_content"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_01_content"
    ).content

    template_opts["contract_termination_02_header"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_02_header"
    ).content

    template_opts["contract_termination_02_content"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_02_content"
    ).content

    template_opts["contract_termination_03_header"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_03_header"
    ).content

    template_opts["contract_termination_03_content"] = CompanyCmsGeneral.objects.get(
        name="contract_termination_03_content"
    ).content

    return HttpResponse(template.render(template_opts, request))


def contract_termination_form(request):
    template = loader.get_template(
        "django_company_cms_general/contract_termination_form.html"
    )

    if request.method == "POST":
        contract_termination_form = ContractTerminationForm(request.POST)

        if contract_termination_form.is_valid():
            try:
                ticket = send_contract_termination_ticket(contract_termination_form)
            except Exception as e:
                messages.error(
                    request, _("Sorry, there was an issue processing the contact data.")
                )
            else:
                messages.success(
                    request,
                    _(
                        "Thank you for your inquiry. We will contact you as soon as possible.".format(
                            ticket
                        )
                    ),
                )

    template_opts = dict()

    template_opts["content_title_main"] = _("Contracts")
    template_opts["content_title_sub"] = _("Contract Termination Form")

    contract_termination_form = ContractTerminationForm
    template_opts["contract_termination_form"] = contract_termination_form

    return HttpResponse(template.render(template_opts, request))
