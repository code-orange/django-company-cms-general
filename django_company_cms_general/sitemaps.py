from django.contrib.sitemaps import Sitemap
from django.urls import reverse


class GeneralStaticViewSitemap(Sitemap):
    def items(self):
        return [
            "general",
            "general_imprint",
            "general_privacy_policy",
            "general_terms_and_conditions",
            "general_contact",
            "general_web_directory",
        ]

    def location(self, item):
        return reverse(item)
