from datetime import datetime

from django.contrib.sites.models import Site
from django.db import models
from parler.models import TranslatedFields, TranslatableModel


class CmsSiteProfile(models.Model):
    site = models.OneToOneField(
        Site, on_delete=models.CASCADE, unique=True, related_name="profile"
    )
    facebook = models.URLField(null=True, blank=True)
    twitter = models.URLField(null=True, blank=True)
    instagram = models.URLField(null=True, blank=True)
    linkedin = models.URLField(null=True, blank=True)
    xing = models.URLField(null=True, blank=True)
    remote_support = models.URLField(null=True, blank=True)
    support_toolbox = models.URLField(null=True, blank=True)

    class Meta:
        db_table = "django_company_cms_site_profile"


class CmsFiles(models.Model):
    bytes = models.BinaryField()
    filename = models.CharField(max_length=255)
    mimetype = models.CharField(max_length=50)

    class Meta:
        db_table = "django_company_cms_files"


class CompanyCmsGeneral(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=250, unique=True)

    translations = TranslatedFields(
        content=models.TextField(),
        translation_needed=models.BooleanField(default=True),
    )

    def __str__(self):
        return self.name

    class Meta:
        db_table = "django_company_cms_general"


class CompanyCmsTestimonial(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    full_name = models.CharField(max_length=50)

    portrait = models.ImageField(
        upload_to="django_company_cms_general.CmsFiles/bytes/filename/mimetype",
        blank=False,
        null=False,
    )

    translations = TranslatedFields(
        content=models.TextField(),
        title=models.CharField(max_length=30, default="CEO"),
    )

    class Meta:
        db_table = "django_company_cms_testimonial"


class CompanyCmsDownloads(TranslatableModel):
    id = models.BigAutoField(primary_key=True)
    unique_name = models.CharField(max_length=50, unique=True)

    thumbnail = models.ImageField(
        upload_to="django_company_cms_general.CmsFiles/bytes/filename/mimetype",
        blank=False,
        null=False,
    )

    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        description=models.TextField(),
        content=models.BinaryField(),
    )

    class Meta:
        db_table = "django_company_cms_downloads"


class CompanyCmsPressReferenceTopic(models.Model):
    name = models.CharField(max_length=100, unique=True)

    class Meta:
        db_table = "django_company_cms_press_reference_topic"


class CompanyCmsPressReference(TranslatableModel):
    date = models.DateField(default=datetime.now)
    topic = models.ForeignKey(CompanyCmsPressReferenceTopic, models.DO_NOTHING)

    translations = TranslatedFields(
        name=models.CharField(max_length=250),
        url=models.URLField(max_length=2083),
    )

    class Meta:
        db_table = "django_company_cms_press_reference"
        ordering = ["-date"]


class CompanyCmsMoreInfoContact(models.Model):
    name = models.CharField(max_length=100, unique=True)
    image = models.CharField(max_length=100, unique=True)

    class Meta:
        db_table = "django_company_cms_more_info_contact"


class CompanyCmsEmailBlacklist(models.Model):
    email = models.EmailField(max_length=100, unique=True)
    silent = models.BooleanField(default=True)

    class Meta:
        db_table = "django_company_cms_email_blacklist"


class CompanyCmsWebDirectory(TranslatableModel):
    slug = models.CharField(max_length=20, unique=True)

    translations = TranslatedFields(
        title=models.CharField(max_length=250),
        description=models.CharField(max_length=2048),
        url=models.URLField(max_length=2083),
    )

    class Meta:
        db_table = "django_company_cms_web_directory"
