from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="general"),
    path(
        "customer-support-guide",
        views.customer_support_guide,
        name="general_customer_support_guide",
    ),
    path("imprint", views.imprint, name="general_imprint"),
    path("privacy-policy", views.privacy_policy, name="general_privacy_policy"),
    path(
        "terms-and-conditions",
        views.terms_and_conditions,
        name="general_terms_and_conditions",
    ),
    path("contact", views.contact, name="general_contact"),
    path(
        "contract-termination",
        views.contract_termination,
        name="general_contract_termination",
    ),
    path(
        "contract-termination/form",
        views.contract_termination_form,
        name="general_contract_termination_form",
    ),
    path("web-directory", views.web_directory, name="general_web_directory"),
    path("error-page/400", views.bad_request, name="bad_request"),
    path("error-page/403", views.permission_denied, name="permission_denied"),
    path("error-page/404", views.page_not_found, name="page_not_found"),
    path("error-page/500", views.server_error, name="server_error"),
]
