# Generated by Django 3.2 on 2021-04-24 00:00

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "django_company_cms_general",
            "0004_companycmstestimonial_companycmstestimonialtranslation",
        ),
    ]

    operations = [
        migrations.AddField(
            model_name="companycmstestimonialtranslation",
            name="title",
            field=models.CharField(default="CEO", max_length=30),
        ),
    ]
