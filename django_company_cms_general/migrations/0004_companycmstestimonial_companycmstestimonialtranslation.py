# Generated by Django 3.2 on 2021-04-23 22:51

from django.db import migrations, models
import django.db.models.deletion
import parler.fields
import parler.models


class Migration(migrations.Migration):
    dependencies = [
        ("django_company_cms_general", "0003_use_bigint_for_id"),
    ]

    operations = [
        migrations.CreateModel(
            name="CompanyCmsTestimonial",
            fields=[
                ("id", models.BigAutoField(primary_key=True, serialize=False)),
                ("full_name", models.CharField(max_length=50)),
                (
                    "portrait",
                    models.ImageField(
                        upload_to="django_company_cms_general.CmsFiles/bytes/filename/mimetype"
                    ),
                ),
            ],
            options={
                "db_table": "django_company_cms_testimonial",
            },
            bases=(parler.models.TranslatableModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name="CompanyCmsTestimonialTranslation",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "language_code",
                    models.CharField(
                        db_index=True, max_length=15, verbose_name="Language"
                    ),
                ),
                ("content", models.TextField()),
                (
                    "master",
                    parler.fields.TranslationsForeignKey(
                        editable=False,
                        null=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="translations",
                        to="django_company_cms_general.companycmstestimonial",
                    ),
                ),
            ],
            options={
                "verbose_name": "company cms testimonial Translation",
                "db_table": "django_company_cms_testimonial_translation",
                "db_tablespace": "",
                "managed": True,
                "default_permissions": (),
                "unique_together": {("language_code", "master")},
            },
            bases=(parler.models.TranslatedFieldsModelMixin, models.Model),
        ),
    ]
