from base64 import b64encode

from django.template import engines
from django.utils.translation import gettext as _

from django_company_cms_general.django_company_cms_general.models import (
    CompanyCmsDownloads,
    CompanyCmsEmailBlacklist,
)
from django_simple_notifier.django_simple_notifier import plugin_zammad
from django_simple_notifier.django_simple_notifier.models import SnotifierTemplate


class TempContact:
    def __init__(self, email):
        self.email = email


def setup_attachment(filename: str, data):
    attachment = dict()
    attachment["filename"] = filename
    attachment["data"] = b64encode(data).decode("utf-8")
    attachment["mime-type"] = "application/pdf"

    return attachment


def contact_inquiry_with_image_docs(name, from_email, phone, message):
    # Blacklist check
    try:
        bl_entry = CompanyCmsEmailBlacklist.objects.get(email__iexact=from_email)
    except CompanyCmsEmailBlacklist.DoesNotExist:
        pass
    else:
        if bl_entry.silent is True:
            return False
        else:
            raise Exception

    note = ""
    note += _("Contact Inquiry") + "\n"
    note += _("Name") + ": " + name + "\n"
    note += _("Phone") + ": " + phone + "\n"
    note += _("E-Mail") + ": " + from_email + "\n"
    note += _("Message") + ": " + message + "\n"

    django_engine = engines["django"]

    template_mail = django_engine.from_string(
        SnotifierTemplate.objects.get(name="contact_inquiry").template
    )

    email_body = template_mail.render(
        {
            "name": name,
            "from_email": from_email,
            "phone": phone,
            "message": message,
        }
    )

    ticket_attachments = list()

    image_folder = CompanyCmsDownloads.objects.get(
        unique_name="DolphinIT_Folder_Image.pdf"
    )

    ticket_attachments.append(
        setup_attachment(
            image_folder.unique_name,
            image_folder.content,
        )
    )

    ticket = plugin_zammad.send(
        [
            TempContact(
                email=from_email,
            )
        ],
        _("Contact Inquiry") + " - " + name,
        email_body,
        group="Sales",
        note=note,
        ticket_id=None,
        main_address=from_email,
        attachments=ticket_attachments,
        content_type="text/html",
    )

    return ticket


def send_contract_termination_ticket(termination_form):
    # Blacklist check
    try:
        bl_entry = CompanyCmsEmailBlacklist.objects.get(
            email__iexact=termination_form.cleaned_data["email"]
        )
    except CompanyCmsEmailBlacklist.DoesNotExist:
        pass
    else:
        if bl_entry.silent is True:
            return False
        else:
            raise Exception

    note = ""
    note += _("Contract Termination") + "\n"
    note += _("Name") + ": " + termination_form.cleaned_data["name"] + "\n"
    note += _("E-Mail") + ": " + termination_form.cleaned_data["email"] + "\n"
    note += (
        _("Customer Number")
        + ": "
        + termination_form.cleaned_data["customer_number"]
        + "\n"
    )
    note += (
        _("Contract Number")
        + ": "
        + termination_form.cleaned_data["contract_number"]
        + "\n"
    )
    note += _("Message") + ": " + termination_form.cleaned_data["message"] + "\n"

    django_engine = engines["django"]

    template_mail = django_engine.from_string(
        SnotifierTemplate.objects.get(name="contract_termination").template
    )

    email_body = template_mail.render(
        {
            "name": termination_form.cleaned_data["name"],
            "from_email": termination_form.cleaned_data["email"],
            "contract_number": termination_form.cleaned_data["contract_number"],
            "message": termination_form.cleaned_data["message"],
        }
    )

    ticket = plugin_zammad.send(
        [
            TempContact(
                email=termination_form.cleaned_data["email"],
            )
        ],
        "{} - {}".format(
            _("Contract Termination"), termination_form.cleaned_data["name"]
        ),
        email_body,
        group="Sales",
        note=note,
        ticket_id=None,
        main_address=termination_form.cleaned_data["email"],
        content_type="text/html",
    )

    return ticket
